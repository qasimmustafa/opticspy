# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 13:51:44 2022

@author: Qasim Mustafa
"""

class Person:
    def __init__(self, name = "Clint", last_name = "Eastwood", birth_year = None):
        self.name = name
        self.last_name = last_name
        self.full_name = None
        self.birth_year = birth_year
    
    def get_full_name(self):
        self.full_name = self.name + self.last_name
        return self.full_name
    
    def calculate_age(self):
        age = 2022 - self.birth_year
        return age


if __name__ == '__main__':
    you = Person('John', 'Wayne')
    print(you.name)
    print(you.last_name)
    print(you.full_name)
    
    him = Person()
    print (him.name)
    print (him.last_name)
    
    him2 = Person(birth_year=1960)
    print(him2.calculate_age())
    print('changes blabla bla')